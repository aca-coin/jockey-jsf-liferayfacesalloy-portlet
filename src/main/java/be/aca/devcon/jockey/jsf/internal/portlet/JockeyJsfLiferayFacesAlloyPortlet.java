package be.aca.devcon.jockey.jsf.internal.portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.Portlet;
import javax.portlet.faces.GenericFacesPortlet;
import javax.servlet.Servlet;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=ACA Devcon",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.ajaxable=false",
		"com.liferay.portlet.requires-namespaced-parameters=false",
		"com.liferay.portlet.css-class-wrapper=jockey-jsf-portlet",
		"javax.portlet.name=jsf",
		"javax.portlet.display-name=Jockey JSF LiferayFacesAlloy Portlet",
		"javax.portlet.init-param.javax.portlet.faces.defaultViewId.view=/WEB-INF/views/jockeys.xhtml",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.portlet-mode=text/html;view"
	},
	service = Portlet.class
)
public class JockeyJsfLiferayFacesAlloyPortlet extends GenericFacesPortlet {

	@Reference(target = "(servlet.init.portlet-class=be.aca.devcon.jockey.jsf.internal.portlet.JockeyJsfLiferayFacesAlloyPortlet)")
	private Servlet servlet;
}
