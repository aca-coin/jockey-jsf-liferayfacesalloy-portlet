package be.aca.devcon.jockey.jsf.internal.bean;

import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;
import com.liferay.faces.util.context.FacesContextHelperUtil;
import com.liferay.faces.util.model.OnDemandDataModel;
import com.liferay.faces.util.model.SortCriterion;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@ManagedBean
@ViewScoped
public class JockeyOnDemandDataModel extends OnDemandDataModel<Jockey> implements Serializable {

	private static final int ROWS_PER_PAGE = 3;

	@ManagedProperty("#{jockeyService}")
	private JockeyService jockeyService;

	@ManagedProperty("#{jockeySearchBean}")
	private JockeySearchBean jockeySearchBean;

	public void search() {
		reset();
		FacesContextHelperUtil.addGlobalSuccessInfoMessage();
	}

	public Collection<Jockey> findRows(int from, int to, List<SortCriterion> criteria) {
		return jockeyService.getJockeys(jockeySearchBean.getTeamName(), from, to + 1);
	}

	public int countRows() {
		return jockeyService.getJockeysCount(jockeySearchBean.getTeamName());
	}

	public int getRowsPerPage() {
		return ROWS_PER_PAGE;
	}

	public void setJockeyService(JockeyService jockeyService) {
		this.jockeyService = jockeyService;
	}

	public void setJockeySearchBean(JockeySearchBean jockeySearchBean) {
		this.jockeySearchBean = jockeySearchBean;
	}
}