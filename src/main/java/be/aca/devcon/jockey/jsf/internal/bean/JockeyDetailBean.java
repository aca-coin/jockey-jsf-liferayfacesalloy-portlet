package be.aca.devcon.jockey.jsf.internal.bean;

import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;
import com.liferay.faces.util.context.FacesContextHelperUtil;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class JockeyDetailBean {

	private static final int DEFAULT_ID = 0;

	private Jockey jockey;

	@ManagedProperty("#{jockeyService}")
	private JockeyService jockeyService;

	@PostConstruct
	public void init() {
		int id = FacesContextHelperUtil.getRequestParameterAsInt("id", DEFAULT_ID);
		if (id != 0) {
			jockey = jockeyService.getJockey(id);
		}
	}

	public Jockey getJockey() {
		return jockey;
	}

	public void setJockey(Jockey jockey) {
		this.jockey = jockey;
	}

	public void setJockeyService(JockeyService jockeyService) {
		this.jockeyService = jockeyService;
	}
}

