package be.aca.devcon.jockey.jsf.internal.bean;

import be.aca.devcon.jockey.service.api.JockeyService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.List;

@ManagedBean
@ViewScoped
public class ApplicationConstants {

	private List<String> teams;

	@ManagedProperty("#{jockeyService}")
	private JockeyService jockeyService;

	@PostConstruct
	public void init() {
		teams = jockeyService.getTeams();
	}

	public List<String> getTeams() {
		return teams;
	}

	public void setJockeyService(JockeyService jockeyService) {
		this.jockeyService = jockeyService;
	}
}
